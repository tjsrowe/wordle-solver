'use strict';

import * as fs from 'fs';
import * as path from 'path';
import * as rd from 'readline';

import { Request, Response } from 'express';
import { addWord, findPossibleWords, parseAbsent, parseHasAt, parseHasIn } from './solver';

import cors from 'cors';
import express from 'express';

const WORDS_FILE = 'data/words.txt';
const LISTEN_PORT = 5120;

const rest = express();
const reader = rd.createInterface(fs.createReadStream(path.join(WORDS_FILE)));

reader.on('line', (l:string) => {
  addWord(l);
});
reader.on('close', () => {
  // runTests();
  createRestServer();
});

function configureCors():any {
  const allowedOrigins = [
    `http://localhost:${LISTEN_PORT}`,
    `https://localhost:${LISTEN_PORT}`
  ];
  const corsOptions = {
    origin: (origin:string, callback:Function) => {
      if (allowedOrigins.includes(origin) || !origin) {
        callback(null, true);
      } else {
        callback(new Error('Origin ' + origin + ' not allowed by CORS'));
      }
    }
  };
  return corsOptions;
}

function createRestServer():void {
  if (rest.get('env') === 'production') {
    rest.set('trust proxy', 1); // trust first proxy
    // TODO sess.cookie.secure = true // serve secure cookies
  }

  const corsOptions = configureCors();

  rest.options('*', cors(corsOptions));

  rest.listen(LISTEN_PORT, () =>
    console.log(`Listening for data model requests on port ${LISTEN_PORT} - http://localhost:${LISTEN_PORT}/`)
  );

  rest.get('/solve/', cors(corsOptions), function(request:Request, response:Response) {
    // let knownLetters:string; // string of character at position 0-4, ? if unknown
    // let possibleLetters:string[]; // array of strings of letters found with position they're not at, 0-4
    // let excludedLetters:string;  // string of letters not in the word

    const hasAt:string|undefined|any = request.query.has;
    const absent:string|undefined|any = request.query.absent;
    const hasIn:string|undefined|any = request.query.hasIn;
    try {
      const possibleLetters:string[] = parseHasIn(hasIn);
      const knownLetters:string = parseHasAt(hasAt);
      const excludedLetters:string = parseAbsent(absent);

      const possibleWords:string[] = findPossibleWords(knownLetters, possibleLetters, excludedLetters);
      response.status(200);
      response.send(possibleWords);
    } catch (err) {
      response.status(400);
      response.send(err);
    }
  });
}
