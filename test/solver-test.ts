import { addWord, findPossibleWords } from '../src/solver';
import assert from 'assert';

function arrayEquals(result:any[], expected:any[]):boolean {
  return result.length === expected.length && result.every((value, index) => value === expected[index]);
}

describe('Basic unit tests', () => {
  describe('findPossibleWords()', () => {
    it('Should have the listed words', function() {
      let result:string[] = [];
      let expected:string[] = [];
      addWord('chart', 'manic', 'mince', 'niece', 'wince', 'brave');

      result = findPossibleWords('f??ny', ['', '', '', 'u', ''], 'stxqplei');
      if (!(result[0] == 'funny' && result.length == 1)) {
        console.warn('Failed.');
      }
      result = findPossibleWords('?????', ['c', '', 'i', 'n', ''], 'sporthk');
      expected = ['manic',
        'mince',
        'niece',
        'wince'
      ];

      if (!arrayEquals(result, expected)) {
        assert.fail('Failed.');
      }
    });
  });
});
