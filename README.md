# README #

This is a little Wordle solver tool which was originally written as a backend service and
can now be used with a React front-end.  The test and production environment are deployed
using Amazon Amplify.

## Production deployment

You can find this tool deployed at https://solvle.tjsr.id.au

## UI Improvements

Trying to find a way to intuitively represent how this tool can be used has been tricky.
If you have suggestions for how to improve the usability and make it easier to understand,
let me know.

## Technology stack

- Typescript
- Express for self-hosted REST api
- React 18 for GUI
- Amazon Amplify for hosted front-end

## Building 

`npm install`
`npm install -g react-scripts`
`npm run build`

## Running the React front-end

`npm start`

When running as a react app, everything will be run on the client - there's no reliance on
hosted service to search and return a word list.

## Back-end

### Running

`ts-node src/solver-server.ts`

### Access

Point a browser at localhost:5120/solve?has=abcde&hasIn=a,b,c,d,e&absent=xyzq

It takes three parameter sets:
- [has], in the format of ????? - replace each ? with a letter which must be present at that position
- [hasIn], comma-separated per letter position
- [absent], rejects any word containing that letter.
